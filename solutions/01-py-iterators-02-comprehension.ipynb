{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Introduction: Comprehending Comprehensions\n",
    "\n",
    "* Comprehensions are a special case of [`list`, `dict` and `set` literals](https://docs.python.org/3/reference/expressions.html#displays-for-lists-sets-and-dictionaries)\n",
    "* Combination of *expression*, *iteration* and *condition*\n",
    "\n",
    "```python\n",
    "fast_list = [ element * random.random() for element in iterable if element >= 3]\n",
    "#            |< expression ----------->|< iteration ---------->|< condition ->|\n",
    "```\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "* Short and readable\n",
    "* Fast and efficient"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "%%timeit values = list(range(200))\n",
    "foo = []\n",
    "for a in values:\n",
    "  foo.append(a)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "%%timeit values = list(range(200))\n",
    "foo = [a for a in values]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Exercise: `list` comprehension\n",
    "\n",
    "The simplest comprehension creates `list` data. Its expression is a single statement, which is stored in the list. The comprehension starts with a `[` and ends with a `]`.\n",
    "\n",
    "<div class=\"alert alert-success\">Assignment:</div>\n",
    "\n",
    "Convert a sequence of float literals to a list of `float`s. Use the list `a1.iterable` as input."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "scrolled": true,
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "from pygks2016.iteration.exercise_a1 import  a1\n",
    "\n",
    "list_of_floats = [float(element) for element in a1.iterable]\n",
    "#             float conversion >|< applied to all elements\n",
    "\n",
    "print('Original:', a1.iterable[:3], '...', type(a1.iterable[0]))\n",
    "print('Result:', list_of_floats[:3], '...', type(list_of_floats[0]))\n",
    "\n",
    "a1.result = list_of_floats"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-info\">Note:</div>\n",
    "\n",
    "List comprehensions are useful for quickly filtering, transforming, and aggregating data. They are powerful e.g. for transforming other lists, or aggregating data from streams and generators."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Exercise: `dict` comprehension\n",
    "\n",
    "Comprehensions for `dict` are more complex: the expression is a `key: value` pair. The comprehension starts with a `{` and ends with a `}`.\n",
    "\n",
    "<div class=\"alert alert-success\">Assignment:</div>\n",
    "\n",
    "Create a ROT13 cipher, mapping each letter of the alphabet to its substitute. Letters are available from `a2.letters`. You can use the function `a2.rot13` for conversion, or write your own.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "from pygks2016.iteration.exercise_a2 import a2\n",
    "\n",
    "# using existing a2.rot13 function\n",
    "# you can inspect its implementation via `a2.rot13??`\n",
    "\n",
    "# expression for dict comprehension like for static literal,\n",
    "# which uses  {key1: value1, key2: value2, ...}\n",
    "\n",
    "rot13_cipher = {letter: a2.rot13(letter) for letter in a2.letters}\n",
    "#                                        ^- performed for all elements\n",
    "#                       ^- map letter via rot13; comes after ':', so it's the value\n",
    "#               ^- key is the unmodified letter\n",
    "\n",
    "a2.result = rot13_cipher"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-info\">Note:</div>Dict comprehensions have the same advantage as List comprehensions. Dicts are more suitable if data or data access is unstructured and unordered.\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true,
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Info: Nested comprehensions\n",
    "\n",
    "Comprehensions can be chained to iterate over multiple iterables. Consider this nested loop:\n",
    "\n",
    "```python\n",
    "data = {}\n",
    "for iterable in iterables:\n",
    "  if iterable:\n",
    "    for key, value in iterable:\n",
    "      if value > 3:\n",
    "        data[key] = value\n",
    "```\n",
    "\n",
    "It can be rewritten as a single comprehension.\n",
    "\n",
    "```python\n",
    "data = {key: value for iterable in iterables if iterable for key, value in iterable if value > 3}\n",
    "```\n",
    "\n",
    "<div class=\"alert alert-info\">Note:</div>\n",
    "\n",
    "The `for ...` and `if ...` statements use the same order in the explicit and comprehension version!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Exercise: Nested set comprehension\n",
    "\n",
    "Putting items into a `set` is a quick way to eliminate duplicates. Its comprehension is like a mix of `list` and `dict` comprehension: the expression the same as for a `list`, and the comprehension is surrounded by `{...}` as for a `dict`.\n",
    "\n",
    "<div class=\"alert alert-success\">Assignment:</div>\n",
    "\n",
    "Use a `set` comprehension to get the unique values of type `int` in all iterables of `a3.iterables`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "from pygks2016.iteration.exercise_a3 import a3\n",
    "\n",
    "unique_ints = {element for sequence in a3.iterables for element in sequence if isinstance(element, int)}\n",
    "#              ^- accept all valid elements unconverted                        ^- only `int` type is valid\n",
    "#                      ^- first unpack *outer* iterable\n",
    "#                                                   ^- then unpack each contained iterable\n",
    "\n",
    "a3.result = unique_ints"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Exercise: Parsing sequence of data to a `dict`\n",
    "\n",
    "There is another way of nesting comprehensions: a comprehension's value or iterable expression may also be a comprehension.\n",
    "\n",
    "<div class=\"alert alert-success\">Assignment: (Advanced)</div>\n",
    "\n",
    "Read the ini-style string `a4.config_str` to a dictionary."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "from pygks2016.iteration.exercise_a4 import a4\n",
    "\n",
    "# this exercise is meant to give a feeling how comprehensions\n",
    "# can be used as input to other comprehensions\n",
    "\n",
    "# if you are unsure about what's going on, write it as multiple\n",
    "# regular for loops first\n",
    "\n",
    "config_dict = {  # split onto multiple lines for readability - it's one statement!\n",
    "        key.strip(): [value.strip() for value in values.split(', ')]  # `key: value` expression\n",
    "#                    ^- value list parsed with comprehension\n",
    "#       ^- single keys; need to strip optional whitespace around `=` (also for values)\n",
    "        for key, values  # iterate over key, values pairs\n",
    "        in # produced by *another* comprehension\n",
    "        [line.split('=', 1) for line in a4.config_str.splitlines() if line]\n",
    "#                                       ^- split into lines        ^- only work with non-empty lines\n",
    "#        ^- split the line at `'='`, so containing comprehension gets key, values\n",
    "    }\n",
    "\n",
    "a4.result = config_dict"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-success\">Note:</div>\n",
    "\n",
    "Avoid deep nesting of comprehensions in shared\\* code! Complex constructs offer little speed advantage - but are difficult to understand, debug and maintain.\n",
    "\n",
    "\\* That includes code shared with future-you."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Conclusions: Comprehensions\n",
    "\n",
    "* Special kind of `dict`, `set`, and `list` literals\n",
    "* Efficient and concise for short transformations and filters\n",
    "* Any *iterable* object compatible as input\n",
    "\n",
    "# [Next Section: Generators](./01-py-iterators-03-generators.ipynb)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}

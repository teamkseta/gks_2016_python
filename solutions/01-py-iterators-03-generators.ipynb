{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Introduction: General Generators\n",
    "\n",
    "* Iterables so far: iterate on containers\n",
    "   * All values explicitly stored/referenced\n",
    "* Generators generalize iteration\n",
    "   * Potentially store *nothing*\n",
    "   * Values *generated* on demand\n",
    "   * Iterate lazily, infinitely, randomly, ...\n",
    "* Many inbuilt helpers are generators\n",
    "   * `range`, `enumerate`, ..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Info: Generator Expression\n",
    "\n",
    "* Equivalent of comprehension is the [generator expression](https://docs.python.org/3/reference/expressions.html#generator-expressions)\n",
    "* Creates a single-use iterable\n",
    "\n",
    "```python\n",
    "generator = ( element * random.random() for element in iterable if element >= 3)\n",
    "#           |< expression ----------->|< iteration ---------->|< condition ->|\n",
    "```\n",
    "\n",
    "<div class=\"alert alert-info\">Note:</div>\n",
    "\n",
    "The parentheses, i.e. `(` and `)`, can be omitted on calls with only one argument."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Info: Why Generators"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "import random\n",
    "repetitions = int(1E6)\n",
    "random.seed(1337); print(sum([random.random() for _ in range(repetitions)])/repetitions)\n",
    "random.seed(1337); print(sum(random.random() for _ in range(repetitions))/repetitions)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "from gks2016rpylib.tools import getbytesizeof\n",
    "print('Size list:', getbytesizeof([random.random() for _ in range(repetitions)]), ', nums:', getbytesizeof(0.5) * repetitions)\n",
    "print('Size genx:', getbytesizeof((random.random() for _ in range(repetitions))), ', nums:', getbytesizeof(0.5))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-danger\">Warning:</div>\n",
    "\n",
    "This kind of generator is *single use* only. For small sequences with negligible memory overhead, generators are usually slower."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "my_gen = (a for a in range(5))\n",
    "print(*('%d: %s' % (idx+1, list(my_gen)) for idx in range(4)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Exercise: Generator Expression\n",
    "\n",
    "Generator expressions are ideal if you need the transformation of a comprehension, but never store the result.\n",
    "\n",
    "<div class=\"alert alert-success\">Assignment:</div>\n",
    "\n",
    "Calculate the standard deviation of `b1.sequence` using generator expressions for transformations."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "from math import sqrt\n",
    "from pygks2016.iteration.exercise_b1 import b1\n",
    "\n",
    "print('Sequence:', ['%.2f' % element for element in b1.sequence[:10]], '...')\n",
    "\n",
    "# first calculate the mean\n",
    "# for each element, calculate it difference to the mean, take the square and sum it up\n",
    "#     all still needs to be normalized\n",
    "# finally, calculate the square root\n",
    "\n",
    "mean = sum(b1.sequence)/len(b1.sequence)\n",
    "std_dev = sqrt(sum((element - mean) ** 2 for element in b1.sequence)/len(b1.sequence))\n",
    "\n",
    "print('Stats:', '%.2f' % mean, '+/-', '%.2f' % std_dev)\n",
    "\n",
    "b1.result = std_dev"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Info: Generator Functions\n",
    "\n",
    "Generator functions do not `return` once, but may [`yield` control](https://docs.python.org/3/reference/expressions.html#yield-expressions) multiple times to create an iterable.\n",
    "\n",
    "Calling a generator function actually creates an iterable. This iterable gets its values from executing the function body. The `yield` keyword *suspends execution* and returns a value. When iterating further, execution is resumed at the last `yield`, until the function ends."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "def generator_function():\n",
    "    print('>> Function: Started')\n",
    "    yield 0\n",
    "    print('>> Function: Resumed')\n",
    "    yield 1\n",
    "    print('>> Function: Exhausted')\n",
    "\n",
    "print('Loop: before start')\n",
    "for generated_value in generator_function():\n",
    "    print('Loop: got', generated_value)\n",
    "print('Loop: after finish')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-info\">Note:</div>\n",
    "\n",
    "The generator function corresponds to the generator expression. Calling it corresponds to evaluating the generator expression. A generator function can be called multiple times to get a \"fresh\" iterator."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Exercise: Fixed Generator Function\n",
    "\n",
    "The simplest generator function produces fixed values.\n",
    "\n",
    "<div class=\"alert alert-success\">Assignment:</div>\n",
    "\n",
    "Write a generator function that yields the vocals `a`, `e`, `i`, `o` and `u`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "from pygks2016.iteration.exercise_b2 import b2\n",
    "\n",
    "def vocal_gen():\n",
    "    \"\"\"yield each vocal once\"\"\"\n",
    "    yield 'a'\n",
    "    yield 'e'\n",
    "    yield 'i'\n",
    "    yield 'o'\n",
    "    yield 'u'\n",
    "\n",
    "for value in vocal_gen():\n",
    "    print(value, end=' ')\n",
    "\n",
    "b2.result = vocal_gen"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Exercise: Generating Values\n",
    "\n",
    "Generators are ideal for leazily generating values. This allows them to be technically *infinite*: additional values may be calculated as required.\n",
    "\n",
    "<div class=\"alert alert-success\">Assignment:</div>\n",
    "\n",
    "Create a generator for the Fibonacci sequence `1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, ...`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "from pygks2016.iteration.exercise_b3 import b3\n",
    "\n",
    "\n",
    "def fibonacci_gen():\n",
    "    \"\"\"\n",
    "    Generator for Fibonacci sequence\n",
    "    \n",
    "    The n-th call returns the sum of the (n-1)th and (n-2)th call.\n",
    "    \"\"\"\n",
    "    next_fib, prev_fib = 1, 0  # need to remember 2 previous values\n",
    "    while True:  # produce infinite stream\n",
    "        yield next_fib  # yield current value, then prepare next one\n",
    "        next_fib, prev_fib = next_fib + prev_fib, next_fib\n",
    "\n",
    "b3.result = fibonacci_gen"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-info\">Note:</div>\n",
    "\n",
    "Generating values is not restricted to calculating them by yourself. Generator code is free to e.g. query web servers, poll sensors, read files, or use random data."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Exercise: Recursive Generation\n",
    "\n",
    "Generator functions support parameters, like regular functions. Also like functions, generator functions can be used as parameters to other (generator) functions!\n",
    "\n",
    "This allows generator functions to forward and modify the object stream of an iteration.\n",
    "\n",
    "<div class=\"alert alert-success\">Assignment: (You don't have to finish them all!)</div>\n",
    "\n",
    "Create several generators, each performing *one* operation for cleaning a stream from a mangled csv file.\n",
    "\n",
    "* Discard empty lines, as well as linebreaks(`'\\n'`)\n",
    "* Join continuation lines. A line starting with a space (` `) must be joined to the *previous* line with a separator.\n",
    "* Split each *joined* line at a separator.\n",
    "* Convert all values to python types. Note: they are valid literals."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "from pygks2016.iteration.exercise_b4 import b4\n",
    "from ast import literal_eval\n",
    "\n",
    "# the generators are chained together to work\n",
    "# on a stream of lines from a file\n",
    "# the line stream is generated from the file\n",
    "# at b4.titanic_data, by calling `open(b4.titanic_data)`\n",
    "\n",
    "def clean_lines(line_iter):\n",
    "    \"\"\"Remove line comments and empty lines\"\"\"\n",
    "    for line in line_iter:\n",
    "        line = line.split('#', 1)[0]  # remove anything after a '#'\n",
    "        line = line.rstrip()  # remove any trailing whitespace\n",
    "        if line:  # skip empty lines (test as False)\n",
    "            yield line  # yield each stripped, non-empty lines\n",
    "\n",
    "\n",
    "def fix_whitespace(line_iter, separator=','):\n",
    "    \"\"\"Remove linebreaks inside fields\"\"\"\n",
    "    # lines starting with ' ' still belong the the current field\n",
    "\n",
    "\n",
    "def fix_whitespace(line_iter, separator=','):\n",
    "    \"\"\"\n",
    "    Remove linebreaks inside fields\n",
    "    \n",
    "    :param line_iter: iterator over content lines\n",
    "    :param separator: separator with which to join multi-line fields\n",
    "    \"\"\"\n",
    "    # need to remember previous, unfinished fields\n",
    "    # explicitly fetch first line with `next`\n",
    "    line_buffer = next(line_iter)\n",
    "    for line in line_iter:\n",
    "        # lines starting with whitespace are continuation\n",
    "        # need to join them with buffered data\n",
    "        if line.startswith(' '):\n",
    "            line_buffer += separator + line.strip()\n",
    "        # when there is no new data, the line is complete\n",
    "        else:\n",
    "            yield line_buffer\n",
    "            line_buffer = line\n",
    "\n",
    "\n",
    "def split_fields(line_iter, separator=','):\n",
    "    \"\"\"\n",
    "    Split lines into individual fields\n",
    "    \n",
    "    :param line_iter: iterator over content lines\n",
    "    :param separator: separator between fields\n",
    "    \"\"\"\n",
    "    # this is a straightforward operation applied to every element\n",
    "    # we can delegate this to a generator expression, or write\n",
    "    # it as an explicit for loop from which we yield\n",
    "    yield from (line.split(separator) for line in line_iter)\n",
    "\n",
    "# One of the fields uses aliases for boolean values.\n",
    "# different values to tell something is true\n",
    "trueish = ('Survived', True, 'Yes', 1)\n",
    "# different values to tell something is false\n",
    "falseish = ('Unaccounted', False, 'No', 0)\n",
    "\n",
    "\n",
    "def parse_fields(elements_iter):\n",
    "    \"\"\"\n",
    "    Parse the fields in each line to python values\n",
    "    \n",
    "    :param elements_iter: iterator over lists of values\n",
    "    \"\"\"\n",
    "    # use `literal_eval` here\n",
    "    # also clean trueish values\n",
    "    # and finally yield your lines\n",
    "    for elements in elements_iter:\n",
    "        # literal_eval safely evaluates literals in strings, e.g. `\"1.03\"` => float\n",
    "        line = [literal_eval(element) for element in elements]\n",
    "        # need to fix boolean aliases\n",
    "        line[3] = line[3] in trueish\n",
    "        yield line\n",
    "\n",
    "b4.result = (clean_lines, fix_whitespace, split_fields, parse_fields)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Conclusions: Generators\n",
    "\n",
    "* Iterables evaluating each value on demand\n",
    "* Create iterables by calculating/fetching values\n",
    "* Modify iterators like a stream\n",
    "\n",
    "## [Next Section: Iterable Classes](./01-py-iterators-04-iterables.ipynb)\n",
    "\n",
    "## Further Reading\n",
    "\n",
    "* Create coroutines by [passing values into generators](https://docs.python.org/3/howto/functional.html#passing-values-into-a-generator)"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}

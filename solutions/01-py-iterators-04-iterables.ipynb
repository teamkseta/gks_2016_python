{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Introduction: Iterable Magic\n",
    "\n",
    "* Iterables so far:\n",
    "   * Builtin Types/Container\n",
    "   * Generators to create/modify iterator\n",
    "* Build your own Container/Iterable/...\n",
    "   * Modify content access, e.g. `OrderedDict`, load balancing, ...\n",
    "   * Specialized data structure, e.g. graphs, trees, multisets, ...\n",
    "   * Abstract containers, e.g. data on the web, storage server, ..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-danger\">Warning:</div>\n",
    "\n",
    "Custom containers using extensive logic in Python\\* are generally slow! Use custom containers with small, thin interfaces or when performance is not critical.\n",
    "\n",
    "\\* This is true when using **CPython**. Other Python implementations, such as **pypy** or **Cython**, *can* get en par with compiled languages."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true,
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Info: Magic `__iter__` is Magic `__iter__`\n",
    "\n",
    "* Python generalizes operator overloading via special \"magic\" methods\n",
    "* Sequence of magic methods is used to implement operatorions\n",
    "\n",
    "```python\n",
    "def bool(obj):\n",
    "    try:\n",
    "        return obj.__bool__()\n",
    "    except AttributeError:\n",
    "        try:\n",
    "            return obj.__len__() != 0\n",
    "        except AttributeError:\n",
    "            return True\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* Iteration is provided by `__iter__` (or falls back to `__getitem__`, ...)\n",
    "* The `__iter__` method is called without parameters, and should return an iterator."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true,
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Exercise: Ring Buffer\n",
    "\n",
    "Custom containers are often built around a *builtin* container. Special methods modify its behaviour, while keeping performance high.\n",
    "\n",
    "In this example, you'll be collecting data from a dummy probe `c1.probe`. However, it cannot be polled, only subscribed to with a container. This fills up regular containers too fast.\n",
    "\n",
    "<div class=\"alert alert-success\">Assignment:</div>\n",
    "\n",
    "Write a ring buffer class to subscribe to the probe.\n",
    "\n",
    "* Takes one parameter named `maxlen`, the maximum buffer size.\n",
    "* Provides `def append(self, value):` to add data.\n",
    "* Provides `def __iter__(self):`, a *generator function* that destructively iterates over data.\n",
    "* Optional: Provides `def __len__(self):`, returning the number of buffered items.\n",
    "* Optional: Provides `def extend(self, iterable)` to add data from an iterable."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "from pygks2016.iteration.exercise_c1 import c1\n",
    "\n",
    "# demonstration of probe and expected solution\n",
    "# containers to hold data\n",
    "regular_container = []\n",
    "sample_container = c1.sample_solution(maxlen=2)\n",
    "\n",
    "# subscribe both containers to the probe\n",
    "c1.probe.subscribe(regular_container, 6).join()\n",
    "c1.probe.subscribe(sample_container, 6).join()\n",
    "\n",
    "# check remaining data\n",
    "print('Regular:', list(regular_container))\n",
    "print('Sample:', list(sample_container))  # implicitly calling iter(sample_container)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "from collections import deque\n",
    "from pygks2016.iteration.exercise_c1 import c1\n",
    "\n",
    "\n",
    "class RingBuffer(object):\n",
    "    \"\"\"\n",
    "    FIFO Ring Buffer with destructive iteration\n",
    "\n",
    "    :param maxlen: maximum size of the buffer before discarding items\n",
    "    :type maxlen: int\n",
    "    \"\"\"\n",
    "    def __init__(self, maxlen=10):\n",
    "        # internal buffer holding our data\n",
    "        # deque with maxlen will automatically drop oldest\n",
    "        # data to make room for more\n",
    "        self._buffer = deque(maxlen=maxlen)\n",
    "\n",
    "    def append(self, value):\n",
    "        \"\"\"Push a value into the buffer. Discards the oldest value if full.\"\"\"\n",
    "        # forward data to internal buffer\n",
    "        self._buffer.append(value)\n",
    "\n",
    "    def __iter__(self):\n",
    "        while self._buffer:\n",
    "            yield self.pop()\n",
    "\n",
    "    def pop(self):\n",
    "        \"\"\"Remove and return the oldest value from the buffer.\"\"\"\n",
    "        # pop from the *left* (append is to the *right*)\n",
    "        # this gets the *oldest* value and removes it from the buffer\n",
    "        return self._buffer.popleft()\n",
    "\n",
    "    def extend(self, iterable):\n",
    "        \"\"\"Push all values from iterable into the buffer. Discards the oldest value(s) if full.\"\"\"\n",
    "        # we could iterate over iterable and append each item\n",
    "        # better to use existing interface if possible\n",
    "        self._buffer.extend(iterable)\n",
    "\n",
    "    def __len__(self):\n",
    "        # magic method called by len and bool operations\n",
    "        # return length of buffer\n",
    "        return len(self._buffer)\n",
    "\n",
    "    def __str__(self):\n",
    "        # nice formatting for your class when printed\n",
    "        return '<%s]' % ', '.join(repr(item) for item in self._buffer)\n",
    "\n",
    "    def __repr__(self):\n",
    "        # technical formatting for your class for debug output\n",
    "        return '%s(%s)' % (self.__class__.__name__, self)\n",
    "\n",
    "\n",
    "class FastRingBuffer(RingBuffer):\n",
    "    \"\"\"\n",
    "    Optimized implementation of :py:class:`~.RingBuffer`\n",
    "    \n",
    "    This class uses thinner interfaces around the builtin,\n",
    "    and has an optimized `__iter__` method.\n",
    "    \n",
    "    This class inherits from :py:class:`~.RingBuffer` so\n",
    "    that its methods are visible on the class. Instances\n",
    "    will shadow these class-defined methods with aliases\n",
    "    to their underlying buffer.\n",
    "    \"\"\"\n",
    "    def __init__(self, maxlen=None):\n",
    "        super().__init__(maxlen=maxlen)\n",
    "        # directly expose the buffer methods on the\n",
    "        # wrapper with aliases\n",
    "        self.append = self._buffer.append\n",
    "        self.pop = self._buffer.popleft\n",
    "        self.extend = self._buffer.extend\n",
    "        self.__len__ = self._buffer.__len__\n",
    "\n",
    "    def __iter__(self):\n",
    "        pop = self.pop # local rebinding saves repeated method lookup\n",
    "        try:  # avoid checking for content - pop breaks the loop when empty\n",
    "            while True:\n",
    "                yield pop()\n",
    "        except IndexError:\n",
    "            return\n",
    "\n",
    "    # avoid inheriting representations from the underlying data structure\n",
    "    # unless they are *very* similar; otherwise, you confuse people\n",
    "    def __str__(self):\n",
    "        # nice formatting for your class when printed\n",
    "        return '<%s]' % ', '.join(repr(item) for item in self._buffer)\n",
    "\n",
    "    def __repr__(self):\n",
    "        # technical formatting for your class for debug output\n",
    "        return '%s(%s)' % (self.__class__.__name__, self)\n",
    "    \n",
    "c1.result = RingBuffer\n",
    "c1.result = FastRingBuffer"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Conclusions: Iterable Classes\n",
    "\n",
    "* Any class can be made iterable\n",
    "* Wrap builtins, create your own data structure, ...\n",
    "\n",
    "## Conclusions: Iterators\n",
    "\n",
    "* Comprehensions provide short, fast conversions from iterables to `list`, `dict` and `sets`\n",
    "* Generators allow creating, filtering, inspecting, (etc.) iterables\n",
    "* Iterable classes allow creating your own interfaces, protocols, (etc.) for data\n",
    "\n",
    "## [Next Block: Enclose, Mutate, Release](../02-py-resources-01-intro.ipynb)\n",
    "\n",
    "## Further Reading\n",
    "\n",
    "* Other magic defind by the [python data model](https://docs.python.org/3.5/reference/datamodel.html)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}

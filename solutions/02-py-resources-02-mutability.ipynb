{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Managing Mutability\n",
    "\n",
    "* Names always refer to the object they are bound to\n",
    "* Targets are changed by explicitly rebinding/releasing a name\n",
    "   * Names are not pointers! Changing a name never has sideeffects on other names\n",
    "* But we can modify objects\n",
    "   * And namespaces are objects, too..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Info: Mutable and Immutable objects\n",
    "\n",
    "* Mutable objects can change their state, immutable objects cannot\n",
    "* Example: numbers are immutable"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "one = 1\n",
    "two = one\n",
    "two += 1 ## oops, let's fix that\n",
    "print(one, two)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* Example: mutable and immutable sequence"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "foo, bar = (), []\n",
    "foo2, bar2 = foo, bar\n",
    "print('Original ', id(foo), foo, id(bar), bar)\n",
    "print('Reference', id(foo2), foo2, id(bar2), bar2)\n",
    "foo2 += (2,)\n",
    "bar2 += [2]\n",
    "print('Original ', id(foo), foo, id(bar), bar)\n",
    "print('Reference', id(foo2), foo2, id(bar2), bar2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Free Exercise: Playing with Mutability\n",
    "\n",
    "Python *never* implicitly copies mutable objects. You may be dragging the same `list` from start to end of your application. This is at the same time\n",
    "\n",
    "1. very efficient, because no time is spent on duplicating data\n",
    "2. an accident waiting to happen, if you are not aware of it\n",
    "\n",
    "<div class=\"alert alert-success\">Assignment:</div>\n",
    "\n",
    "Experiment with mutability and immutability. Try assigning objects to multiple names, modify one and inspect the other. You can also try `dis.dis` to see what is going on.\n",
    "\n",
    "* How does augmented assignment (`+=`) behave? Is `a += b` the same as `a = a + b`?\n",
    "* How names/items inside a containers and namespace behave?\n",
    "   * Use the `exercise_a1` module as a namespace; we've import its content also as local names (via `... import *`).\n",
    "* You can check the identity of two objects via `one_object is another_object`.\n",
    "* The function `id(obj)` gives you the memory location of an object (in CPython).\n",
    "* Define a function with a mutable object as a default parameter. Modify this parameter in the function.\n",
    "\n",
    "**Feel free to ask for help, explanation or discussion. We get cookies for doing this course.**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "These samples \"solutions\" just include *some* of the effects of mutability.\n",
    "Feel free to experiment/google around on your own"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "import dis\n",
    "from pygks2016.resources import exercise_a1\n",
    "from pygks2016.resources.exercise_a1 import *\n",
    "\n",
    "print('Module vs local:', exercise_a1.a_string is a_string)\n",
    "dis.dis('a, b = a_list, a_string')\n",
    "\n",
    "def test_func(param=[]):\n",
    "    pass"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 58,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Tuple: (1, 2, 3, 4, 5) (1, 2, 3) immutable\n",
      "List : [1, 2, 3, 4, 5] [1, 2, 3, 4, 5] mutable\n",
      "Str  : 12345 123 immutable\n",
      "Set  : {'4', '1', '3', '5', '2'} {'4', '1', '3', '5', '2'} mutable\n",
      "Num  : 1 2 immutable\n"
     ]
    }
   ],
   "source": [
    "# augmented assignment:\n",
    "a = (1, 2, 3)\n",
    "b = a\n",
    "a += (4, 5)\n",
    "print('Tuple:', a, b, 'immutable')\n",
    "\n",
    "a = [1, 2, 3]\n",
    "b = a\n",
    "a += [4, 5]\n",
    "print('List :', a, b, 'mutable')\n",
    "\n",
    "a = \"123\"\n",
    "b = a\n",
    "a += \"45\"\n",
    "print('Str  :', a, b, 'immutable')\n",
    "\n",
    "# sets are 'dicts without values', same applies to both\n",
    "a = {k for k in \"123\"}\n",
    "b = a\n",
    "a.update({k for k in \"45\"})\n",
    "print('Set  :', a, b, 'mutable')\n",
    "\n",
    "# this would be VERY strange if not for immutability\n",
    "one = 1\n",
    "two = one  # oops\n",
    "two += 1   # let's fix that...\n",
    "print('Num  :', one, two, 'immutable')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Many containers are *mutable*, because you probably need to extend/modify them. Since classes/instances use `dicts` to store their attributes, they are mutable as well! Primitives, such as `1`, `True`, etc., are *not* mutable.\n",
    "\n",
    "`str`, `tuple` and `frozenset` are the most prominent immutable containers. This feature makes them useable as keys in a `dict` or elements in a `set`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[:]  : [1, 1] [1, 1]\n",
      "[:]  : [(1, 2, 3, 4, 5), (1, 2, 3)] [(1, 2, 3, 4, 5), (1, 2, 3)]\n",
      "List*: ['a', 'a', 'a']\n",
      "Nest : [[], [], []]\n",
      "Nest : [['to idx 0'], ['to idx 0'], ['to idx 0']]\n"
     ]
    }
   ],
   "source": [
    "# inside containers\n",
    "ca = [None, None]\n",
    "cb = ca\n",
    "ca[:] = [1, 1]  # [from:to:stride] => [:] replace all elements\n",
    "print('[:]  :', ca, cb)  # mutable container (list) changes visilbe across names\n",
    "\n",
    "ca[0] = (1, 2, 3)\n",
    "ca[1] = ca[0]\n",
    "ca[0] += (4, 5)\n",
    "print('[:]  :', ca, cb)  # container references work like name references\n",
    "\n",
    "# be mindful of accidentaly cloning object references\n",
    "demo = [\"a\"] * 3\n",
    "print('List*:', demo)\n",
    "\n",
    "nested = [[]] * 3\n",
    "print('Nest :', nested)\n",
    "nested[0].append('to idx 0')\n",
    "print('Nest :', nested)  # reference to inner list has been cloned into each position"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "References from containers behave the same as references from names. Since containers may be mutables, be mindful when having mutables inside of them.\n",
    "\n",
    "Once you get used to stacking mutable objects, it allows for very clean passing of data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 51,
   "metadata": {
    "collapsed": false,
    "scrolled": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Test: Call with defaults\n",
      "(2, (0,), [0], NameSpace({'an_attr1': 1}))\n",
      "(2, (0,), [0, 1], NameSpace({'an_attr1': 1, 'an_attr2': 2}))\n",
      "(2, (0,), [0, 1, 2], NameSpace({'an_attr1': 1, 'an_attr2': 2, 'an_attr3': 3}))\n",
      "--> Changes to mutable default arguments PERSIST!\n",
      "Test: Call with identical parameters\n",
      "(2, (0,), [0], NameSpace({'an_attr1': 1}))\n",
      "(2, (0,), [0, 1], NameSpace({'an_attr1': 1, 'an_attr2': 2}))\n",
      "(2, (0,), [0, 1, 2], NameSpace({'an_attr1': 1, 'an_attr2': 2, 'an_attr3': 3}))\n",
      "--> Same with using identical objects for each call!\n",
      "Test: Call with new parameters\n",
      "(2, (0,), [0], NameSpace({'an_attr1': 1}))\n",
      "(2, (0,), [0], NameSpace({'an_attr1': 1}))\n",
      "--> New parameters don't see previous actions.\n",
      "Test: why?\n",
      "4360411216 4412259968 4412426120 4412384480\n",
      "4360411216 4412335216 4412426120 4412384480\n",
      "--> Mutable defaults are the identical object on every call!\n",
      "Test: why is that?\n",
      "(a_primitive=1, a_tuple=(), a_list=[0, 1, 2, 3, 4], an_object=NameSpace({'an_attr4': 4, 'an_attr5': 5, 'an_attr1': 1, 'an_attr2': 2, 'an_attr3': 3}))\n",
      "4360411184 4361060424 4412426120 4412384480\n",
      "--> Functions are partially defined by their signatures.\n",
      "--> The signature is evaluated only once, when creating the function.\n"
     ]
    }
   ],
   "source": [
    "# Using functions with *mutable* defaults can be tricky\n",
    "# If you come from another language, the behaviour is\n",
    "# likely confusing at first.\n",
    "\n",
    "class NameSpace:\n",
    "    \"\"\"Empty class capable of holding attributes\"\"\"\n",
    "    def __repr__(self):\n",
    "        return \"%s(%s)\" % (self.__class__.__name__, {attr: getattr(self, attr) for attr in dir(self) if attr.startswith('an_attr')})\n",
    "\n",
    "def demo_func(a_primitive=1, a_tuple=(), a_list=[], an_object=NameSpace()):\n",
    "    \"\"\"Demonstrator for default argument mutation\"\"\"\n",
    "    a_primitive += 1\n",
    "    a_tuple += (len(a_tuple), )\n",
    "    a_list += [len(a_list)]\n",
    "    setattr(an_object, 'an_attr%d' % len(a_list), len(a_list))\n",
    "    return a_primitive, a_tuple, a_list, an_object\n",
    "\n",
    "print('Test: Call with defaults')\n",
    "print(demo_func())\n",
    "print(demo_func())\n",
    "print(demo_func())  \n",
    "print('--> Changes to mutable default arguments PERSIST!')\n",
    "\n",
    "print('Test: Call with identical parameters')\n",
    "new_args = 1, (), [], NameSpace()\n",
    "print(demo_func(*new_args))\n",
    "print(demo_func(*new_args))\n",
    "print(demo_func(*new_args))\n",
    "print('--> Same with using identical objects for each call!')\n",
    "\n",
    "print('Test: Call with new parameters')\n",
    "print(demo_func(1, (), [], NameSpace()))\n",
    "print(demo_func(1, (), [], NameSpace()))  # explicitly repeating parameters creates new ones on every call\n",
    "print('--> New parameters don\\'t see previous actions.')\n",
    "\n",
    "print('Test: why?')\n",
    "print(*(id(result) for result in demo_func()))\n",
    "print(*(id(result) for result in demo_func()))\n",
    "print('--> Mutable defaults are the identical object on every call!')\n",
    "\n",
    "print('Test: why is that?')\n",
    "import inspect\n",
    "print(inspect.signature(demo_func))\n",
    "print(*(id(default.default) for default in inspect.signature(demo_func).parameters.values()))\n",
    "print('--> Functions are partially defined by their signatures.')\n",
    "print('--> The signature is evaluated only once, when creating the function.')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Mutability of function defaults is considered practically **the** gotcha of python, *if* you are experienced with classical languages. The effect follows from everything being an object in python: that includes the function, its defaults and signature.\n",
    "\n",
    "If you want mutable defaults that do not persist, you can easily implement that. The following construct makes it clear to other programmers that you want a *new* default list on every call:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 59,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "4412423112\n",
      "4412424008\n",
      "4412342088\n",
      "--> Creating the default value inside the body (if needed) gets you a new object every time.\n"
     ]
    }
   ],
   "source": [
    "def new_mutable_demo_func(a_list=None):  # use None as placeholder to signal \"no value given\"\n",
    "    a_list = a_list if a_list is not None else ['a default value', 'another default value']\n",
    "    return a_list\n",
    "\n",
    "print(id(new_mutable_demo_func()))\n",
    "print(id(new_mutable_demo_func()))\n",
    "print(id(new_mutable_demo_func()))\n",
    "print('--> Creating the default value inside the body (if needed) gets you a new object every time.')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Conclusions: Mutability\n",
    "\n",
    "* Mutability is not a concept exclusive to python\n",
    "   + changing state is often why you have instances in the first place\n",
    "* However, *everything* in python is an object, and only few objects are not mutable.\n",
    "* Be mindful of object identity to avoid unexpected effects across your program\n",
    "* Mutating instead of passing around is very efficient and clean if done right\n",
    "\n",
    "## [Next Section: Cool Closure & Whacky Weakref](./02-py-resources-03-references.ipynb)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
